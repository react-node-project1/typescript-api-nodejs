import * as express from 'express'
import * as path from 'path'
import * as fs from 'fs'
export class Route {
    app: express.Application
    constructor(app: express.Application) {
        this.app = app;
    }
    initRoute(): Boolean {
        const fullLocation = path.join(__dirname, '../routes');
        fs.readdir(fullLocation, (err: any, files: any) => {
            if (err) return;
            files.map((x: any, i: number) => {
                if (i % 2 === 0) { // .js .js.map => eliminate 1 of them
                    return;
                } else {
                    const fileNameWithoutExt = x.split(".")[0]
                    const pathRequire = "../routes/" + fileNameWithoutExt
                    const pathRoute = require(pathRequire)
                    const route = fileNameWithoutExt === "index" ? "/" : "/" + fileNameWithoutExt
                    this.app.use(route, pathRoute);
                }

            })
        })
        return true;
    }
}