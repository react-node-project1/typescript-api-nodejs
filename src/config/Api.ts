
import * as express from 'express'
import * as cors from 'cors'
// import { Server } from "./server"
import { Route } from "./Route"
import * as bodyParser from "body-parser";
import { Database } from '../database/Database'

// import { app } from 'firebase-admin'
export class API {
    app: express.Application = express()
    constructor() {
        this.config(this.app)
    }
    config(app: express.Application): void {
        // sdung các package cần thiết
        app.use(cors({ origin: true }))
        app.use(bodyParser.json()).use(bodyParser.urlencoded({ extended: true }))
        const router = new Route(app);
        const database = new Database()
        database.connect()
        router.initRoute();
        app.get('/dat', (req, res) => {
            console.log("dat");
            res.send('dattttttttttttttttttt')
        })
        // let server = new Server(app)
        // server.initDb(); // Connect to the database.
        // server.initRoutes(); // Use the router location.
        // server.initViews(); // Set up the server views.
        // server.run()
    }
}
