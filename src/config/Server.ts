import * as express from 'express'

export class Server {
    app: express.Application
    constructor(app: express.Application) {
        this.app = app;
    }
    run() {
        this.app.listen(8000)
    }
}