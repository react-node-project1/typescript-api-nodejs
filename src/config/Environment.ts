// More content in config.ts
import { config as configDotenv } from 'dotenv'
import { resolve } from 'path';

// console.log(process.env.DB_HOST)
switch (process.env.NODE_ENV) {
    case undefined:
        console.log("Environment is 'development'")
        configDotenv({
            path: resolve(__dirname, "../.env.development")
        })
        break
    case "test":
        configDotenv({
            path: resolve(__dirname, "../.env.test")
        })
        break
    // Add 'staging' and 'production' cases here as well!
    default:
        throw new Error(`'NODE_ENV' ${process.env.NODE_ENV} is not handled!`)
}
const throwIfNot = <T, K extends keyof T>(obj: Partial<T>, prop: K, msg?: string): T[K] => {
    if (obj[prop] === undefined || obj[prop] === null) {
        throw new Error(msg || `Environment is missing variable ${prop}`)
    } else {
        return obj[prop] as T[K]
    }
}
// Validate that we have our expected ENV variables defined!
['DB_USERNAME', 'DB_PASSWORD', 'PORT', 'DB_HOST', 'DB_DATABASE'].forEach(v => {
    throwIfNot(process.env, v)
})

export interface IProcessEnv {
    DB_USERNAME: string| undefined
    DB_PASSWORD: string| undefined
    PORT: string| undefined
    DB_HOST: string | undefined
    DB_DATABASE: string | undefined
}

declare global {
    namespace NodeJS {
        interface ProcessEnv extends IProcessEnv { }
    }
}