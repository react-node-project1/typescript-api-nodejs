import * as functions from 'firebase-functions';
import * as express from 'express'
import { API } from "./config/Api"
import './config/Environment'
const bootAPI = new API();
const ServerAPI: express.Application = bootAPI.app

export const webApi = functions.https.onRequest(ServerAPI)

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
